#!/usr/bin/env - ruby
require "zlib"
require "fileutils"
require "pp"

class PPD
  attr_reader :path
  attr_reader :version
  attr_reader :model_name

  def initialize(path)
    @path = path

    # Switch based on file extension
    if path.match(/\.gz$/)
      handler = Zlib::GzipReader
    else
      handler = File
    end

    # Extract version and model name information from the PPD
    handler.open(path) do |file|
      file.each_line do |line|
        line.chomp!

        if m = line.match(/\^*FileVersion: "(.*)"/)
          @version = m[1]
        end

        if m = line.match(/\^*ModelName: "(.*)"/)
          @model_name = m[1]
        end

        # Shortcut out
        if @version && @model_name
          break
        end

      end
    end
  end

  def ==(other_ppd)
    self.path == other_ppd.path &&
    self.version == other_ppd.version &&
    self.model_name == other_ppd.model_name
  end

  def to_s
    "#<#{self.class} #{self.model_name}: #{self.path} - #{self.version}>"
  end

  def self.is_newer(a, b)
    a_split = a.version.split(".")
    b_split = b.version.split(".")

    digits = [a_split.count, b_split.count].max

    # Add zeros to both sides
    while (a_split.count < digits)
      a_split << "0"
    end

    while (b_split.count < digits)
      b_split << "0"
    end

    # Compare every position by integer
    (0..digits).each do |i|
      # Digits match, move to next
      next if a_split[i].to_i == b_split[i].to_i

      # Digits don't match, return comparison
      return b_split[i].to_i > a_split[i].to_i
    end

    # Numbers are the same
    return false
  end

end

class PPDUpdater

  def installed_ppds
    return find_ppds_in_directory("/Library/Printers/PPDs/Contents/Resources")
  end

  def installed_printers
    return find_ppds_in_directory("/etc/cups/ppd")
  end

  def find_old_printers(ppds, printers)
    candidates = Array.new
    printers.each do |printer|
      # Find any PPDs for this printer model
      ppds_for_model = ppds.find_all { |ppd| printer.model_name == ppd.model_name }

      # Look through those PPDs for something with a higher version number
      candidate = printer
      ppds_for_model.each do |ppd|
        if PPD.is_newer(printer, ppd) && PPD.is_newer(candidate, ppd)
          candidate = ppd
        end
      end

      # Add to queue
      if candidate != printer
        candidates << { :printer => printer, :driver => candidate }
      end
    end

    return candidates
  end

  private

  def find_ppds_in_directory(path)
    ppds = Array.new
    Dir.glob(File.expand_path(path) + "/**/*.ppd*" ) do |file|
      if File.file?(file)
        ppds << PPD.new(file)
      end
    end

    return ppds
  end


end

updater = PPDUpdater.new
installed_printers = updater.installed_printers()
installed_ppds = updater.installed_ppds()

ppds_to_update = updater.find_old_printers(installed_ppds, installed_printers)

if (ppds_to_update.size > 0)
  ppds_to_update.each do |update|
    source_ppd = update[:driver].path
    destination_ppd = update[:printer].path
    puts "Updating #{destination_ppd} with #{source_ppd}"
    FileUtils.copy(source_ppd, destination_ppd)
  end

  system("/usr/bin/killall", "cupsd")
end

