require_relative "../ppd-updater"

describe PPD do
  it "obtains the version of a PPD" do
    ppd = PPD.new("/Library/Printers/PPDs/Contents/Resources/HP Officejet H470.ppd.gz")
    expect(ppd.version).to eq("4.0.2")
  end

  it "obtains the model name from the PPD" do
    ppd = PPD.new("/Library/Printers/PPDs/Contents/Resources/HP Officejet H470.ppd.gz")
    expect(ppd.model_name).to eq("HP Officejet H470")
  end

  it "should handle non-gzipped PPDs" do
    ppd = PPD.new("/etc/cups/ppd/ptr_usb_2146_01_local.ppd")
    expect(ppd.model_name).to eq("HP LaserJet 4350")
  end

  it "should be able to compare three digit version numbers" do
    old_version = double("PPD", :version => "1.1.1")
    new_version = double("PPD", :version => "1.1.2")
    expect(PPD.is_newer(old_version, new_version)).to be_true

    # Check for false cases
    expect(PPD.is_newer(new_version, old_version)).to be_false

    # Check for different lengths
    old_version = double("PPD", :version => "1")
    new_version = double("PPD", :version => "1.2")
    expect(PPD.is_newer(old_version, new_version)).to be_true

    old_version = double("PPD", :version => "2")
    new_version = double("PPD", :version => "1.2")
    expect(PPD.is_newer(old_version, new_version)).to be_false

    old_version = double("PPD", :version => "1.1.1")
    new_version = double("PPD", :version => "2")
    expect(PPD.is_newer(old_version, new_version)).to be_true

    old_version = double("PPD", :version => "2")
    new_version = double("PPD", :version => "2.1.1")
    expect(PPD.is_newer(old_version, new_version)).to be_true

    old_version = double("PPD", :version => "1.1.1")
    new_version = double("PPD", :version => "2.0")
    expect(PPD.is_newer(old_version, new_version)).to be_true

    new_version = double("PPD", :version => "2.0.0.0")
    expect(PPD.is_newer(old_version, new_version)).to be_true

    new_version = double("PPD", :version => "2.0")
    expect(PPD.is_newer(old_version, new_version)).to be_true

    new_version = double("PPD", :version => "1.2.1")
    expect(PPD.is_newer(old_version, new_version)).to be_true

    new_version = double("PPD", :version => "2.1.1")
    expect(PPD.is_newer(old_version, new_version)).to be_true

    new_version = double("PPD", :version => "10.1.1")
    expect(PPD.is_newer(old_version, new_version)).to be_true

    # Numeric, not lexical comparison
    old_version = double("PPD", :version => "9.1.1")
    new_version = double("PPD", :version => "10.1.1")
    expect(PPD.is_newer(old_version, new_version)).to be_true

    # Check equality
    expect(PPD.is_newer(old_version, old_version)).to be_false
  end

end


describe PPDUpdater do
  it "should find PPDs installed on the system" do
    updater = PPDUpdater.new
    ppd = PPD.new("/Library/Printers/PPDs/Contents/Resources/HP Officejet H470.ppd.gz")
    expect(updater.installed_ppds).to include ppd
  end

  it "should find all the CUPS ppds" do
    updater = PPDUpdater.new
    ppd = PPD.new("/etc/cups/ppd/ptr_usb_2146_01_local.ppd")
    expect(updater.installed_printers).to include ppd
  end

  it "should find any PPDs that are out-of-date" do
    updater = PPDUpdater.new

    # fake version number for testing
    ppd = PPD.new("/etc/cups/ppd/ptr_usb_2146_01_local.ppd")

    installed_ppd = ppd.dup
    installed_ppd.stub(:version) { "2.0" }
    installed_printers = [ installed_ppd ]

    installed_ppds = Array.new # updater.installed_ppds

    low_duplicate = ppd.dup
    low_duplicate.stub(:version) { "1.2" }
    low_duplicate.should_receive(:model_name).at_least(:once).and_call_original
    installed_ppds << low_duplicate

    high_duplicate = ppd.dup
    high_duplicate.stub(:version) { "99.99" }
    high_duplicate.should_receive(:model_name).at_least(:once).and_call_original

    installed_ppds << high_duplicate

    expect(updater.find_old_printers(installed_ppds, installed_printers)).to eq([ [ installed_ppd, high_duplicate ] ])
    expect(updater.find_old_printers(installed_ppds, installed_printers).flatten).to_not include(low_duplicate)

  end

end
